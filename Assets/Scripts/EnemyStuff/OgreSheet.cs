﻿using UnityEngine;
using System.Collections;

public class OgreSheet : MonoBehaviour {

	//Initialize the Ogre's stats
	void Start () {
        Stats stats = GetComponent<Stats>();
        stats.hpregen = 10f;
        stats.strength = 20;
        stats.dexterity = 10;
        stats.constitution = 20;
        stats.intelligence = 5;
        stats.wisdom = 5;
        stats.charisma = 5;
	    
        stats.hpmax = stats.constitution * 10;
        stats.basehpmax = stats.hpmax;
        stats.hp = stats.hpmax;
	    stats.speed = new Vector2(0.1f, 0.1f);
        stats.isControllable = true; 

        WeaponScript ws = GetComponent<WeaponScript>();
        ws.shootingRate = 8f;   
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
