﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour {
    
    private PhotonView pv;
    private float timer;

    public Transform enemyPrefab;    
    public string Enemy = "Ogre";
    public float frequency = 5.0f;    
    public bool isSpawning = true;    
    public int maxEnemies = 1;
    public Transform target { get; set; }
	// Use this for initialization
	void Start () {
        pv = GetComponent<PhotonView>();                      
	}

    void Update()
    {        
        if(isSpawning && transform.childCount < maxEnemies && PhotonNetwork.isMasterClient)
        {
            UpdateSpawner();
        }        
    }
    
        
    void UpdateSpawner()
    {
        timer += Time.deltaTime;
        if (timer > frequency)
        {
            pv.RPC("SpawnEnemy", PhotonTargets.MasterClient);
            timer = 0f;                        
        }
    }

    [RPC]
    void SpawnEnemy()
    {        
        var enemy = PhotonNetwork.Instantiate(Enemy, transform.position, transform.rotation, 0) as GameObject;
        enemy.transform.parent = transform;     
    }
   
}
