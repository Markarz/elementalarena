﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour
{

    public Transform target { get; set; }
    public float moveSpeedBase {get;set;}
    public float moveSpeed;
    public float     rotationSpeed;
    public float maxdistance;
    public float mindistance;    
    public Vector3 dir;    
    private Utils utils;
    private float frequency = 2f;
    private float timer;
    private Stats stats;
    
    void Start()
    {
        utils = FindObjectOfType<Utils>();
        stats = GetComponent<Stats>();
        maxdistance = 3f;
        mindistance = 0.4f;
        moveSpeedBase = moveSpeed;
    }
    

    void Update()
    {
        if (stats.isControllable)
        {

            if (utils != null)
            {
                timer += Time.deltaTime;

                if (timer > frequency)
                {
                    target = utils.FindClosestPlayer().transform;
                    timer = 0f;
                }

                if (target != null)
                {
                    var distance = Vector3.Distance(target.position, transform.position);

                    if (distance > mindistance && distance < maxdistance)
                    {
                        // Get a direction vector from us to the target
                        dir = target.position - transform.position;

                        // Normalize it so that it's a unit direction vector
                        dir.Normalize();

                        // Move ourselves in that direction
                        transform.position += dir * stats.speed.x * Time.deltaTime;
                    }
                }
            }
        }    
    }

    


}


