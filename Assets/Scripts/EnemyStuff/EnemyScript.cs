﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy generic behavior
/// </summary>
public class EnemyScript : MonoBehaviour
{
    private WeaponScript ws;
    private AI ai;
    private Transform[] weapons;
    private PhotonView pv;
    private Stats stats;

	void Awake()
	{
		// Retrieve the weapon only once
	    ai = GetComponent<AI>();
        ws = GetComponent<WeaponScript>();
        pv = GetComponent<PhotonView>();
        stats = GetComponent<Stats>();
        weapons = ws.weapons;
        ws.shootingRate = 5f;	    
	}

    void Update()
    {        
        StartCoroutine(Attack());
    }

    public IEnumerator Attack()
    {
        if (stats.isControllable)
        {
            foreach (Transform weapon in weapons)
            {
                // Auto-fire
                if (weapon != null && ws.CanAttack)
                {
                    //No movement while attacking, and pause for 1s                                
                    ai.moveSpeed = 0f;
                    yield return new WaitForSeconds(2f);
                    ws.EnemyAttack(ai.dir, 20f);
                    ai.moveSpeed = ai.moveSpeedBase;
                }
            }
        }
    }    

    //IEnumerator Attack()
    //{
    //    if (pv.isMine)
    //    {
    //        foreach (Transform weapon in weapons)
    //        {
    //            // Auto-fire
    //            if (weapon != null && ws.CanAttack)
    //            {
    //                //No movement while attacking, and pause for 1s                
    //                ai.moveSpeed = 0f;
    //                yield return new WaitForSeconds(1.5f);
    //                pv.RPC("EnemyAttack", PhotonTargets.All, ai.dir);
    //                ai.moveSpeed = speed;
    //            }
    //        }
    //    }
    //}
}