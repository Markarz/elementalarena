﻿using UnityEngine;

public class WeaponScript : MonoBehaviour
{	
	public Transform shotPrefab;
    public Transform[] weapons;
    //private float shootingRate = 0.25f;
    public float shootingRate { get; set; }
    public float manaCost { get; set; }
    public bool CanAttack { get { return shootCooldown <= 0f; } }

    public float shootCooldown { get; set; }
    private PlayerScript playerscript;
    private HealthScript hs;
			
	void Start()
	{
        shotPrefab = weapons[0];
		shootCooldown = 0f;		
        //animator = GetComponent<Animator>();
        playerscript = GetComponent<PlayerScript>();
	    hs = GetComponent<HealthScript>();
	    shootingRate = 5f;
	}
	
	void Update()
	{		
		shootCooldown -= Time.deltaTime;            		
	}

    [RPC]
    public void Attack(Vector3 direction, float damage, AudioClip ac)
    {                      
        if (CanAttack)
        {            
             //Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;
            
            ShotScript ss = shotTransform.GetComponent<ShotScript>();
            ss.SetID(GetInstanceID());
            ss.damage = damage;

            shotTransform.position = transform.position;
            shotTransform.Rotate(direction);            
            shotTransform.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);           
            shotTransform.GetComponent<ShotScript>().SetID(GetInstanceID());                        

            if(manaCost > 0)
                hs.UseMana(manaCost);

            // Make the weapon shot always towards it
            MoveScript move = shotTransform.GetComponent<MoveScript>();
            if (move != null)
            {
                move.direction = direction;
            }

            GetComponent<AudioSource>().PlayOneShot(ac);
            
            shootCooldown = shootingRate;
        }
    }

    [RPC]
    public void Melee_Attack(Vector3 direction, float damage)
    {
        Debug.Log("melee dir: " + direction);
        if (CanAttack)
        {
            //Create a new shot            
            var shotTransform = Instantiate(shotPrefab) as Transform;
            shotTransform.position = transform.position + (direction * 0.2f);
            shotTransform.Rotate(direction);
            shotTransform.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);
            shotTransform.GetComponent<ShotScript>().SetID(GetInstanceID());

            ShotScript ss = shotTransform.GetComponent<ShotScript>();
            ss.SetID(GetInstanceID());
            ss.damage = damage;

            MoveScript move = shotTransform.GetComponent<MoveScript>();
            if (move != null)
            {
                move.direction = direction;
            }

            shootCooldown = shootingRate;
        }
    }
    public void EnemyAttack(Vector3 direction, float damage)
    {        
        if (CanAttack)
        {            
            //Create a new shot            
            var shotTransform = Instantiate(shotPrefab) as Transform;
            ShotScript ss = shotTransform.GetComponent<ShotScript>();
            ss.SetID(GetInstanceID());
            ss.damage = damage;

            shotTransform.position = transform.position;
            shotTransform.Rotate(direction);
            shotTransform.GetComponent<TeamMember>().SetTeamID(-1);
            //set this shot's id to its weaponscript's id
            shotTransform.GetComponent<ShotScript>().SetID(GetInstanceID());

            if (manaCost > 0)
                hs.UseMana(manaCost);

            // Make the weapon shot always towards it
            MoveScript move = shotTransform.GetComponent<MoveScript>();
            if (move != null)
            {                
                move.direction = direction;
            }

            shootCooldown = shootingRate;
        }
    }

   
    public void Attack_360(bool isEnemy)
    {
        var pos = Camera.main.WorldToScreenPoint(transform.position);
        var dir = Input.mousePosition - pos;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        if (CanAttack)
        {
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;
            shotTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            // The isEnemy property
            ShotScript shot = shotTransform.gameObject.GetComponent<ShotScript>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            // Make the weapon shot always towards it
            MoveScript move = shotTransform.gameObject.GetComponent<MoveScript>();
            if (move != null)
            {
                //move.direction = this.transform.right; // towards in 2D space is the right of the sprite
                move.direction = dir.normalized;
            }
        }
    }
	
}