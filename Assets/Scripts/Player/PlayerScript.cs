﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;

/// <summary>
/// Player controller and behavior
/// </summary>
public class PlayerScript : MonoBehaviour
{    
    private Vector3 right = new Vector3(1, 0, 0);
    private Vector3 left = new Vector3(-1, 0, 180);
    private Vector3 up = new Vector3(0, 1, 90);
    private Vector3 down = new Vector3(0, -1, 270);
    private Vector2 movement;   // 2 - Store the movement
    private Rigidbody2D rigid2d; 

	private float cachedY;
	private float maxX;
	private float minX;


    private WeaponScript weapon;
    private Animator animator;
    private GameObject player;
    private TeamMember team;
    private Vector3 localScale;
    private GenericPowers powers;
    private PhotonView pv;
    private Stats stats; 

    private KnightPowers knight;
    private RangerPowers ranger;
    private DruidSpells druid;
    private WizardSpells wizard; 

    private bool facingUp = true;
    private bool shoot = false;        
    private float inputX = 0;
    private float inputY = 0;
    public bool casting { get; set; }
    
    public Vector3 dir = new Vector3(-1, 0, 180);    
    
    public bool sixteen = true;        
    public bool facingRight = false; ///If a sprite is oriented left, set this to false on the prefab.      
    public CharacterState _characterState;
    public string character;

    public bool isFrozen = false; 
    

    public enum CharacterState
    {
        Idle = 0,
        Walking = 1,
        Trotting = 2,
        Running = 3,
        Jumping = 4,
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");
        team = GetComponent<TeamMember>();
        _characterState = CharacterState.Idle;
        powers = GetComponent<GenericPowers>();
        pv = GetComponent<PhotonView>();
        stats = GetComponent<Stats>();

        knight = GetComponent<KnightPowers>();
        ranger = GetComponent<RangerPowers>();
        wizard = GetComponent<WizardSpells>();
        druid = GetComponent<DruidSpells>();

        rigid2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (stats.isControllable)
        {
            //Movement
            inputX = Input.GetAxis("Horizontal");
            inputY = Input.GetAxis("Vertical");
            Move(inputX, inputY);
            rigid2d.WakeUp();
            rigid2d.velocity = movement;             
            Move_Flip();
            //dir = FireAtMouse();
        }
        else
        {
            rigid2d.Sleep();
        }
    }

    void Update()
    {
        if (stats.isControllable)
        {
            weapon = GetComponent<WeaponScript>();
            shoot = Input.GetButton("Fire1"); //allow user to hold the button
            shoot |= Input.GetButton("Fire2");
            StartCoroutine(Shoot(shoot));
        }
    }

    void Flip()
    {
        localScale = transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    public IEnumerator Shoot(bool shoot)
    {
        //Must edit the shooting rate on the prefab. Shootcooldown is taken from the prefabs, overwrites the val in code. 
        if (shoot)
        {            
            // Auto-fire
            if (weapon != null && weapon.CanAttack && pv.isMine && !casting)
            {
                stats.isControllable = false;
                if (character == "Red_Wizard")
                {
                    wizard.Wizard_Attack();
                }                
                if (character == "Ranger")
                {
                    ranger.Ranger_Attack(dir);
                }
                if (character == "Knight")
                {
                    knight.Knight_Attack(dir);
                }
                if(character == "Druid")
                {
                    druid.Druid_Attack(dir);
                }                
                yield return new WaitForSeconds(weapon.shootingRate * 0.5f);
                stats.isControllable = true; 
            }            
        }
    }

    void Move(float inputX, float inputY)
    {
        //LookAtMouse();
        // 4 - Movement per direction        
        movement = new Vector2(stats.speed.x*inputX, stats.speed.y*inputY);        
    }

    void Move_Flip()
    {        
        //walk right
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            if (!facingRight)
            {                
                Flip();
            }

            dir = right;
            facingRight = true;            
        }

        //walk left
        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            if (facingRight)
            {
                Flip();
            }

            dir = left;
            facingRight = false;
        }

        //walk up
        if (Input.GetAxisRaw("Vertical") > 0)
        {
            //if (!facingUp)
            //{
                facingUp = true;
                dir = up;
            //}
        }
        //walk down
        if (Input.GetAxisRaw("Vertical") < 0)
        {
            //if (facingUp)
            //{
                facingUp = false;
                dir = down;
            //}
        }
    }

    void Move_sixteen()
    {        
        //walk right
        if (Input.GetAxisRaw("Horizontal") > 0)
        {            
            animator.SetBool("right", true);
            animator.SetBool("left", false);
            animator.SetBool("up", false);
            animator.SetBool("down", false);
            dir = right;            
        }        

        //walk left
        if (Input.GetAxisRaw("Horizontal") < 0)
        {         
            animator.SetBool("right", false);
            animator.SetBool("left", true);
            animator.SetBool("up", false);
            animator.SetBool("down", false);            
            dir = left;            
        }        
        //walk up
        if (Input.GetAxisRaw("Vertical") > 0)
        {            
            animator.SetBool("right", false);
            animator.SetBool("left", false);
            animator.SetBool("up", true);
            animator.SetBool("down", false);
            dir = up;            
        }        
        //walk down
        if (Input.GetAxisRaw("Vertical") < 0)
        {            
            animator.SetBool("right", false);
            animator.SetBool("left", false);
            animator.SetBool("up", false);
            animator.SetBool("down", true);
            dir = down;             
        }        
    }

    public void Cast(string effect, float CastTime)
    {
        if (pv.isMine)
        {
            casting = true;
            stats.isControllable = false;             
            pv.RPC(effect, PhotonTargets.All, CastTime);
        }
    }
    public void PostCast()
    {
        stats.isControllable = true; 
        casting = false;
    }



    void LookAtMouse()
    {
        var pos = Camera.main.WorldToScreenPoint(transform.position);
        var dir = Input.mousePosition - pos;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    
    Vector3 FireAtMouse()
    {
        var pos = Camera.main.WorldToScreenPoint(transform.position);
        var dir = Input.mousePosition - pos;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        var rot = Quaternion.AngleAxis(angle, Vector3.forward);        
        return dir.normalized;
    }
    
}