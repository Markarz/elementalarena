﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour
{
    
    private float frequency = 5.0f;
    private float timer;
    private float manaFrequency = 5.0f;
    private float manaTimer;
    PhotonView pv;
    private GameObject player;
    private HealthScript hs;                

    //basic attributes
    public int strength = 10;
    public int dexterity = 10;
    public int constitution = 10;
    public int wisdom = 10;
    public int intelligence = 10;
    public int charisma = 10;

    public Vector2 basespeed = new Vector2(1f, 1f);
    public Vector2 speed = new Vector2(1f, 1f);

    public float basecastingspeed { get; set; }  
    public float castingspeed { get; set; }

    private float _hp;
    public float hp
    {
        get
        {            
            return _hp; 
        }

        set
        {
            _hp = value;
            hs.UpdateHealthBar();
            Debug.Log("_hp: " + _hp);
        }
    }
        
    public float basehpmax = 100f;
    public float hpmax = 100f; 
    public float hpregen = 0f;

    public float mana = 100f;    
    public float manamax = 100f;
    public float basemanamax = 100f;
    public float manaregen = 0f;

    public float PercentReduction = 0f;
    public float dmgReducedByHit = 0f;

    public float basecritchance = 0.05f;
    public float critchance = 0.05f;
    public float critbonus = 1.5f;
    public float[] damage;
    public bool isControllable = false;

    public float experience = 0;

	void Start () {
        player = GameObject.FindWithTag("Player");
        pv = GetComponent<PhotonView>();
        hs = GetComponent<HealthScript>();

        InitializeResources();
	}

    public void InitializeResources()
    {
        hp = constitution * 5;
        hpmax = constitution * 5;
        hpregen = constitution * 0.5f; 

        mana = intelligence * 5;
        manamax = intelligence * 5;
        manaregen = intelligence * 0.5f;        
    }
	// Update is called once per frame
	void Update ()
	{
        Regenerate(hpregen);
        Regenerate_Mana(manaregen);
	}   

    public void UpdateStats(int str = 0, int dex = 0, int con = 0, int wis = 0, int intel = 0, Vector2 speedmod = default(Vector2))
    {
        if (con != 0)
        {
            constitution += con;
            hpmax += (con * 5);
            hp += (con * 5);
            hpregen += (con * 0.5f); 
        }
        if (intel != 0)
        {
            intelligence += intel;
            manamax += (intel * 5);
            mana += (intel * 5);
            manaregen += (intel * 0.5f);
        }
        //if (speedmod != default(Vector2))
        //{
        //    //if a negative is passed in, we end up with +=-, which subtracts
        //    //var newspeed = speed;
        //    //newspeed.x += (speedmod);
        //    //newspeed.y += (speedmod);
        //    //speed = newspeed;
        //    Debug.Log("wizard speed: " speed + " " + speedmod);
        //    speed += speedmod;
        //}        
    }

    public void Regenerate(float amount)
    {
        timer += Time.deltaTime;
        if (timer > frequency)
        {
            hs.AddHealth(amount);
            timer = 0f;
        }
    }

    public void Regenerate_Mana(float amount)
    {
        manaTimer += Time.deltaTime;

        if (manaTimer > manaFrequency)
        {
            hs.AddMana(amount);
            manaTimer = 0f;
        }
    }
    
    public float CalculateWeaponDamage()
    {
        float dmg = 0f;
        bool isCrit = false;
        float crit = Random.Range(0, 1f);

        if (crit < critchance)
        {
            //crit, use max damage for this hit and multiply the damage
            dmg = damage[1];
            isCrit = true;
        }
        else
        {
            dmg = Random.Range(damage[0], damage[1]);
        }

        //add bonuses from stats

        //add bonuses from equipment

        //add bonuses from buffs

        //add bonuses from crit
        if (isCrit)
            dmg *= critbonus;

        return dmg;
    }

    //Use
    public float CalculateDamage(float minDmg, float maxDmg)
    {
        float dmg = 0f; 
        bool isCrit = false;        
        float crit = Random.Range(0, 1f);

        if (crit < critchance)
        {
            //crit, use max damage for this hit and multiply the damage at the end
            dmg = maxDmg;
            isCrit = true;
        }
        else
        {
            dmg = Random.Range(minDmg, maxDmg);
        }

        //add bonuses from stats

        //add bonuses from equipment

        //add bonuses from buffs

        //add bonus from crit
        if (isCrit)
            dmg *= critbonus;

        return dmg;
    }

    
}
