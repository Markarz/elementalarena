﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float distance = -1;
	// Use this for initialization
	void Start ()
	{
	}
	
    void Update()
    {
        transform.position = new Vector3(target.position.x, target.position.y, -1);
    }
}
