﻿using UnityEngine;
using System.Collections;

public class Utils : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    public IEnumerator Enable(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        gameObject.SetActive(true);
    }

    public GameObject FindClosestPlayer()
    {
        var players = GameObject.FindGameObjectsWithTag("Player");
        var distance = 0f;
        GameObject curPlayer = gameObject;

        if (players != null && players.Length > 0)
        {
            foreach (GameObject player in players)
            {                
                Vector3 distanceVector = player.transform.position - transform.position;
                if (distanceVector.sqrMagnitude > distance)
                {
                    distance = distanceVector.sqrMagnitude;
                    curPlayer = player;
                }
            }
        }
        
        return curPlayer;
    }    
    
}
