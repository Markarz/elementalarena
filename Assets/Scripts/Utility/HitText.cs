﻿using UnityEngine;
using System.Collections;

public class HitText : MonoBehaviour
{
    public Color color;
    public float scroll = 0.05f;  // scrolling velocity
    public float duration = 1.5f; // time to die
    public float alpha;
    private Vector3 curPos;
    
    void Start()
    {
        GetComponent<GUIText>().material.color = color; // set text color        
        alpha = 1;
    }
 
    void Update()
    {
        if (alpha>0)
        {
            curPos = transform.position;
            curPos.y += scroll * Time.deltaTime;
            //transform.position.y += scroll*Time.deltaTime;             
            alpha -= Time.deltaTime/duration;
            color = new Color(color.r, color.g, color.b, alpha);            
            GetComponent<GUIText>().material.color = color;
            GetComponent<GUIText>().transform.position = curPos;            
        }
        else
        {
            Destroy(gameObject); // text vanished - destroy itself
        }
    }
}
