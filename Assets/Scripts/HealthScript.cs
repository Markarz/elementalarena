﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Handle hitpoints and damages
/// </summary>
public class HealthScript : MonoBehaviour
{		  
    //This hit list prevents a target from being hit by area effect more than one time 
    //before the object is destroyed.
    public Stack hitList = new Stack();    
    private PhotonView view;    
    private WeaponScript ws;
    private Stats stats;
    public bool canBeFrozen = true; 	
    
    public static Image HealthBar; 	
    private float _healthPercentage = 1f;

    public int playerid { get; set; }

    enum damageTypes 
    {
        heal,
        damage,
        poison, 
        mana, 
        local
    }

    public Transform hitPointsPrefab;
    
    void Start()
    {
        view = gameObject.GetComponent<PhotonView>();        
        ws = GetComponent<WeaponScript>();
        stats = GetComponent<Stats>();        
    }

    public void UpdateHealthBar()
    {
        var hp = stats.hp;
        if (hp > 0)
        {
            _healthPercentage = stats.hp/stats.hpmax;    
        }

        HealthBar.fillAmount = _healthPercentage;
    }

    private void SpawnDamageGui(float points, float x, float y,  damageTypes type)
    {
        x = Mathf.Clamp(x, 0.05f, 0.95f); // clamp position to screen to ensure
        y = Mathf.Clamp(y, 0.05f, 0.9f);  // the string will be visible
        Transform gui = Instantiate(hitPointsPrefab, new Vector3(x, y, 0), Quaternion.identity) as Transform;                
        
        int fontSize = (int) Mathf.Clamp(points * 2, 25, 250);        
        gui.GetComponent<GUIText>().fontSize = fontSize;        
        
        if (points > 0 && type == damageTypes.damage && playerid == 0)
            gui.GetComponent<GUIText>().GetComponent<HitText>().color = Color.yellow;
        else
            gui.GetComponent<GUIText>().GetComponent<HitText>().color = Color.red;
        
        if(points < 0 || type == damageTypes.heal)
        {
            if (points < 0)
                points *= -1;

            gui.GetComponent<GUIText>().GetComponent<HitText>().color = Color.green;
        }

        gui.GetComponent<GUIText>().text = points.ToString();        
    }

    [RPC]
    public void Damage(float amount)
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        if(playerid == 0)        
            SpawnDamageGui(Mathf.RoundToInt(amount), pos.x, pos.y, damageTypes.damage);
        else
            SpawnDamageGui(Mathf.RoundToInt(amount), pos.x, pos.y, damageTypes.local);

        stats.hp -= amount;
        if (stats.hp <= 0)
        {
            StartCoroutine(Die());
        }
    }

    [RPC]
    public void AddHealth(float amount)
    {
        if (amount > 0)
        {
            Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);            
            SpawnDamageGui(Mathf.RoundToInt(amount), pos.x, pos.y, damageTypes.heal);            

            if (stats.hp + amount > stats.hpmax)
                stats.hp = stats.hpmax;
            else
                stats.hp += amount;
        }     		 
    }    

	

    //We only need a to access mana locally at this point. 
    public void AddMana(float amount)
    {        
        if (stats.mana + amount > stats.manamax)
            stats.mana = stats.manamax;
        else
            stats.mana += amount;
    }
    public bool UseMana(float amount)
    {        
        //Can't use the ability unles you have enough mana
        if (stats.mana - amount < 0)
            return false;

        stats.mana = (stats.mana - amount);
        return true;
    }

    void OnTriggerEnter2D(Collider2D otherCollider)
    {        
        ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript>();
        TeamMember tm = otherCollider.GetComponent<TeamMember>();
        TeamMember myTm = GetComponent<TeamMember>();
        PlayerScript ps = GetComponent<PlayerScript>();
        var rpc = "Damage";
        if (shot != null && PhotonNetwork.isMasterClient)
        {            
            // Avoid friendly fire                                                                  

            if (tm == null || myTm == null
                || tm.teamID == 0 || myTm.teamID == 0 //covers renegades
                || tm.teamID != myTm.teamID //not on same team
                )
            {
                //case where we take damage                
                //As %red increases, take the remaining %. 0.4f = (1 - 0.4f) => 0.6f * dmg => taking 60% dmg
                float modifiedDamage = (shot.damage * (1 - stats.PercentReduction)) - stats.dmgReducedByHit;

                if (shot.heal)
                {
                    modifiedDamage *= -1;
                    rpc = "AddHealth";
                }

                if (hitList.Count == 0)
                {                    
                    hitList.Push(shot.gameObject.GetInstanceID());
                    view.RPC(rpc, PhotonTargets.All, modifiedDamage);
                }
                else if (!hitList.Contains(shot.gameObject.GetInstanceID()))
                {
                    if (hitList.Count > 15)
                        hitList.Pop();
                    hitList.Push(shot.gameObject.GetInstanceID());
                    view.RPC(rpc, PhotonTargets.All, modifiedDamage);
                }                
            }
            else
            {
                //Take no damage
            }
        }            
        if (!shot.selfDestruct && ws.GetInstanceID() != shot.id)
        {
            // Remember to always target the game object, otherwise you will just remove the script                                        
            if (shot.networked)
            {
                Debug.Log("netwrok destroy");
                PhotonNetwork.Destroy(shot.gameObject);
            }
            else
            {
                Debug.Log("normal destroy");
                Destroy(shot.gameObject);
            }
            //destroy shot only if it won't destroy itself quickly, for AE dmg.
        }
    }

    [RPC]
    public void DestroyRPC(GameObject go)
    {
        Destroy(go);
    }

    private IEnumerator Die()
    {                
        PhotonView pv = GetComponent<PhotonView>();        
        if (pv.instantiationId == 0)
        {            
            Destroy(gameObject);
        }
        else
        {            
            if(pv.isMine)
            {
                PhotonNetwork.Destroy(gameObject);
                pv.RPC("DeathFX", PhotonTargets.All, 2f);
            
                if (gameObject.tag == "Player")
                {
                    NetworkManager nm = FindObjectOfType<NetworkManager>();
                    nm.standbyCamera.SetActive(true);
                    nm.AddChatMessage(PlayerPrefs.GetString("Username", "Player") + " died. Respawning in 10 seconds.");
                    nm.respawnTimer = 10f;
                    nm.startTimer = true;            

                    yield return new WaitForSeconds(10f);

                    nm.standbyCamera.SetActive(false);
                    nm.SpawnMyPlayer(nm.teamID);                                                
                }                
            }            
        }    
    }

    public IEnumerator FreezeEffect(Collider2D target, Vector2 SpeedReduction, float Duration)
    {        
        if (canBeFrozen)
        {
            canBeFrozen = false; 
            var stats = target.GetComponent<Stats>();
            var pv = target.GetComponent<PhotonView>();
            stats.UpdateStats(speedmod: SpeedReduction);
            pv.RPC("ConeOfColdFX", PhotonTargets.All, Duration);            
            yield return new WaitForSeconds(Duration);            
            stats.UpdateStats(speedmod: -SpeedReduction);
            yield return new WaitForSeconds(15f); 
            canBeFrozen = true; 
        }
    }                       		
}