﻿using UnityEngine;
using System.Collections;

public class FxManager : MonoBehaviour
{
    public GameObject brokenwallPrefab;
    public GameObject blinkPrefab;    
    public GameObject castPrefab;
    public GameObject Cast_MeteorPrefab;
    public GameObject coneofcoldPrefab;    
    public GameObject deathPrefab;
    public GameObject effectPrefab;    
    public GameObject fortifyPrefab;    
    public GameObject healPrefab;    
    public GameObject lifetapPrefab;        
    public GameObject shapeshiftPrefab;    
    public GameObject stunnedPrefab;


    public Transform FireballPrefab;


    public AudioClip blinkClip;
    public AudioClip ConeClip;
    public AudioClip fortifyClip;
    public AudioClip fireballClip;
    public AudioClip healClip;
    public AudioClip lifetapClip;
    public AudioClip MeteorClip;
    public AudioClip sprintClip;
   
    //ranger audio     
    public AudioClip bowAudio;
    public AudioClip spearAudio;
    public AudioClip bulletAudio;

    void Start()
    {
        var scripts = GameObject.FindGameObjectWithTag("scripts").GetComponent<FxManager>();
        brokenwallPrefab = scripts.brokenwallPrefab;
        healPrefab = scripts.healPrefab;
        lifetapPrefab = scripts.lifetapPrefab;
        blinkPrefab = scripts.blinkPrefab;
        castPrefab = scripts.castPrefab;
        shapeshiftPrefab = scripts.shapeshiftPrefab;
        fortifyPrefab = scripts.fortifyPrefab;
        effectPrefab = scripts.effectPrefab;
        Cast_MeteorPrefab = scripts.Cast_MeteorPrefab;
        deathPrefab = scripts.deathPrefab;
        stunnedPrefab = scripts.stunnedPrefab;
        coneofcoldPrefab = scripts.coneofcoldPrefab;

        FireballPrefab = scripts.FireballPrefab;

        blinkClip = scripts.blinkClip;
        ConeClip = scripts.ConeClip;
        fortifyClip = scripts.fortifyClip;
        healClip = scripts.healClip;
        lifetapClip = scripts.lifetapClip;
        MeteorClip = scripts.MeteorClip;
        sprintClip = scripts.sprintClip;

        bowAudio = scripts.bowAudio;
        spearAudio = scripts.spearAudio;
        bulletAudio = scripts.bulletAudio;

        
        
        
    }

    [RPC]
    public void ConeOfColdFX(float Duration)
    {        
        StartCoroutine(ConeOfColdEffect(Duration));
    }

    public IEnumerator ConeOfColdEffect(float Duration)
    {
        var oldcolor = gameObject.GetComponent<Renderer>().material.color;
        gameObject.GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(Duration);
        gameObject.GetComponent<Renderer>().material.color = oldcolor;
    }

    [RPC]
    void StunnedFX(Vector3 position, float duration)
    {
        if (stunnedPrefab == null)
        {
            Debug.Log("No stunned object.");
        }

        GameObject go = Instantiate(stunnedPrefab, position, Quaternion.identity) as GameObject;
        go.GetComponent<ShotScript>().timetolive = duration; 
    }

    [RPC]
    void DeathFX( float duration)
    {
        if (deathPrefab == null)
        {
            Debug.Log("No death object.");
        }

        GameObject go = Instantiate(deathPrefab, transform.position, Quaternion.identity) as GameObject;
    }

    [RPC]
    IEnumerator MeteorCastFX(float Duration, Vector3 position)
    {
        if (Cast_MeteorPrefab == null)
        {
            Debug.Log("No cast_meteor prefab attached to fxmanager.");
        }
        
        GameObject obj = Instantiate(Cast_MeteorPrefab, position, Quaternion.identity) as GameObject;        

        yield return new WaitForSeconds(Duration);
        Destroy(obj);
    }

    [RPC]
    IEnumerator EffectFX(float Duration)
    {
        if (effectPrefab == null)
        {
            Debug.Log("No fortify prefab attached to fxmanager.");
        }
        //Vector3 pos = new Vector3(0f, 0.15f, 0f);
        GameObject obj = Instantiate(effectPrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;

        yield return new WaitForSeconds(Duration);
        Destroy(obj);
    }

    [RPC]
    IEnumerator FortifyFX(float Duration)
    {
        if (fortifyPrefab == null)
        {
            Debug.Log("No fortify prefab attached to fxmanager.");
        }
        Vector3 pos = new Vector3(0f, 0.15f, 0f);
        GameObject obj = Instantiate(fortifyPrefab, transform.position + pos, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;

        yield return new WaitForSeconds(Duration);
        Destroy(obj);
    }

    [RPC]
    IEnumerator ShapeshiftFX(float castTime)
    {
        if (shapeshiftPrefab == null)
        {
            Debug.Log("No casting prefab attached to fxmanager.");
        }

        GameObject obj = Instantiate(shapeshiftPrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;

        yield return new WaitForSeconds(castTime);
        Destroy(obj);
    }
        
    [RPC]
    IEnumerator CastFX(float castTime)
    {
        if (castPrefab == null)
        {
            Debug.Log("No casting prefab attached to fxmanager.");
        }

        GameObject obj = Instantiate(castPrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;

        yield return new WaitForSeconds(castTime);
        Destroy(obj);
    }   

    [RPC]
    IEnumerator HealFX(float Duration)
    {
        if (healPrefab == null)
        {
            Debug.Log("No heal prefab attached to fxmanager.");            
        }

        GameObject obj = Instantiate(healPrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;        

        yield return new WaitForSeconds(Duration);
        Destroy(obj);
    }

    [RPC]
    IEnumerator LifetapFX()
    {
        if (lifetapPrefab == null)
        {
            Debug.Log("No lifetap prefab attached to fxmanager.");
        }

        GameObject obj = Instantiate(lifetapPrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;

        yield return new WaitForSeconds(1.5f);
        Destroy(obj);
    }

    [RPC]
    IEnumerator BlinkFX(float Duration)
    {
        if (blinkPrefab == null)
        {
            Debug.Log("No lifetap prefab attached to fxmanager.");
        }

        GameObject obj = Instantiate(blinkPrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;

        yield return new WaitForSeconds(Duration);
        Destroy(obj);
    }

    [RPC]
    public void LayTemporaryBrokenWall(Vector2 box, Vector3 boxposition, float duration)
    {
        StartCoroutine(LayTile_Respawn(box, boxposition, duration));        
    }
    
    private IEnumerator LayTile_Respawn(Vector2 box, Vector3 boxposition, float duration)
    {                
        ArrayList brokenwalls = new ArrayList();
        if (brokenwallPrefab != null)
        {

            Vector2 boxsize = new Vector2(box.x, box.y); //not scaled down        
            bool negX = false;
            bool negY = false;
            Vector3 boxpositive = boxposition;
            Vector3 newTilePos = boxpositive;
            //check for negatives, store the state and use it to convert back after math is done
            if (boxposition.x < 0)
            {
                negX = true;
                boxpositive.x *= -1;
            }
            if (boxposition.y < 0)
            {
                negY = true;
                boxpositive.y *= -1;
            }

            //get the direction to lay tile
            int tileDir = 0;
            int numTiles = 0;

            if (boxsize.x > boxsize.y)
            {
                tileDir = 1;
                numTiles = (int)(boxsize.x / boxsize.y);
            }
            if (boxsize.y > boxsize.x)
            {
                tileDir = -1;
                numTiles = (int)(boxsize.y / boxsize.x);
            }
            if (boxsize.x == boxsize.y)
            {
                numTiles = 1;
            }

            //Lay tile
            boxsize.x /= (200);
            boxsize.y /= (200);

            for (int i = 0; i < numTiles; i++)
            {
                if (i == 0)
                {
                    newTilePos = new Vector3((boxpositive.x + 0.12f), (boxpositive.y + 0.12f), 10);
                }
                else
                {
                    if (tileDir == 1)
                    {
                        //Add 0.24 in to the x value 
                        newTilePos.x += (float)0.24;
                    }
                    if (tileDir == -1)
                    {
                        //Add 0.24 in the y value
                        newTilePos.y -= (float)0.24;
                    }
                }

                //convert back negatives
                if (negX)
                {
                    newTilePos.x *= -1;
                    negX = false; //reset the bool
                }

                if (negY)
                {
                    newTilePos.y *= -1;
                    negY = false;
                }

                GameObject obj = Instantiate(brokenwallPrefab, newTilePos, Quaternion.identity) as GameObject;
                brokenwalls.Add(obj);
            }

        }
        else
        {
            Debug.Log("Broken wall prefab is missing.");
        }

        yield return new WaitForSeconds(duration);

        if (brokenwalls.Count > 0)
        {
            foreach (GameObject obj in brokenwalls)
            {
                Destroy(obj);
            }                                    
        }

    }
    [RPC]
    void LayTile(Vector2 box, Vector3 boxposition)
    {
        if (brokenwallPrefab != null)
        {

            Vector2 boxsize = new Vector2(box.x, box.y); //not scaled down        
            bool negX = false;
            bool negY = false;
            Vector3 boxpositive = boxposition;
            Vector3 newTilePos = boxpositive;
            //check for negatives, store the state and use it to convert back after math is done
            if (boxposition.x < 0)
            {
                negX = true;
                boxpositive.x *= -1;
            }
            if (boxposition.y < 0)
            {
                negY = true;
                boxpositive.y *= -1;
            }

            //get the direction to lay tile
            int tileDir = 0;
            int numTiles = 0;

            if (boxsize.x > boxsize.y)
            {
                tileDir = 1;
                numTiles = (int) (boxsize.x/boxsize.y);
            }
            if (boxsize.y > boxsize.x)
            {
                tileDir = -1;
                numTiles = (int) (boxsize.y/boxsize.x);
            }
            if (boxsize.x == boxsize.y)
            {
                numTiles = 1;
            }

            //Lay tile
            boxsize.x /= (200);
            boxsize.y /= (200);

            for (int i = 0; i < numTiles; i++)
            {
                if (i == 0)
                {
                    newTilePos = new Vector3((boxpositive.x + 0.12f), (boxpositive.y + 0.12f), 10);
                }
                else
                {
                    if (tileDir == 1)
                    {
                        //Add 0.24 in to the x value 
                        newTilePos.x += (float) 0.24;
                    }
                    if (tileDir == -1)
                    {
                        //Add 0.24 in the y value
                        newTilePos.y -= (float) 0.24;
                    }
                }

                //convert back negatives
                if (negX)
                {
                    newTilePos.x *= -1;
                    negX = false; //reset the bool
                }

                if (negY)
                {
                    newTilePos.y *= -1;
                    negY = false;
                }

                Instantiate(brokenwallPrefab, newTilePos, Quaternion.identity);
            }

        }
        else
        {
            Debug.Log("Broken wall prefab is missing.");
        }

    }
}
