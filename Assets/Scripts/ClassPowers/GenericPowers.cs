﻿using UnityEngine;
using System.Collections;

public class GenericPowers : MonoBehaviour {

    PlayerScript ps;
    HealthScript hs;
    PhotonView pv;
    Stats stats;
    WeaponScript ws;
    FxManager fx; 

    private Animator animator;      
    private string character;    
    private float hp;
    private float hpMax;
    private float hpMod;
    private float hpMaxMod;
    
    private float mana;
    private float manaMax;
    private float manaMod;
    private float manaMaxMod;

    private Vector2 speed;
    private float speedMod;
        
    void Start()
    {
        var scripts = GameObject.FindGameObjectWithTag("scripts");
        ps = GetComponent<PlayerScript>();
        hs = GetComponent<HealthScript>();
        pv = GetComponent<PhotonView>();        
        stats = GetComponent<Stats>();
        animator = GetComponent<Animator>();
        ws = GetComponent<WeaponScript>();        
        character = ps.character;
        fx = GetComponent<FxManager>();

        hp = stats.hp;
        hpMax = stats.hpmax;
        speed = stats.speed;
        mana = stats.mana;
        manaMax = stats.manamax;

    }

    public IEnumerator Heal(float ManaCost, float MinRange, float MaxRange, float CastTime)
    {        
        //Standard 10, heals 13-20 health for 7 mana                
        bool success = hs.UseMana(ManaCost);

        if (success)
        {               
            ps.Cast("CastFX", CastTime);
            yield return new WaitForSeconds(CastTime);
            GetComponent<AudioSource>().PlayOneShot(fx.healClip);

            var amount = Random.Range(MinRange, MaxRange);

            if (pv.isMine)
            {
                pv.RPC("AddHealth", PhotonTargets.All, amount);
                pv.RPC("HealFX", PhotonTargets.All, 2f);
            }

            ps.PostCast();
        }        
    }
           
    //Helper methods

    


    public Vector3 CheckBounds(Vector3 pos)
    {
        Vector3 newpos = new Vector3();

        if (pos.x > 12f)
            newpos = new Vector3(12.2f, pos.y, pos.z);
        if (pos.x < 0.2f)
            newpos = new Vector3(0.2f, pos.y, pos.z);
        if (pos.y < -12.2)
            newpos = new Vector3(pos.x, -12.2f, pos.z);
        if (pos.y > -0.3f)
            newpos = new Vector3(pos.x, -0.3f, pos.z);

        return newpos;
    }

    private delegate void DelayedMethod();


    IEnumerator WaitAndDo(float time, DelayedMethod method)
    {
        yield return new WaitForSeconds(time);
        method();
    }

}
