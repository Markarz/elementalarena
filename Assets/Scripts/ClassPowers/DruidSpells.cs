﻿using UnityEngine;
using System.Collections;

public class DruidSpells : MonoBehaviour {
    
    private Animator animator;
    private GenericPowers gp;
    private HealthScript hs;
    private PlayerScript ps;
    PhotonView pv;
    Stats stats;
    WeaponScript ws;
    public Transform cureLightPrefab;
    public Transform cureLinePrefab;

    private string character;    
    private string curForm = "human";
            
    private float bearReduction = 0.2f;
    private float bearPerHit = 2f;           
    private float birdDuration = 5f;

    private float birdAttackRate = 0.25f; //1 attack / 5 seconds
    private float wolfAttackRate = 0.5f; //4 attacks/3 seconds  
    private float humanAttackRate = 1f; 
    private float bearAttackRate = 1.5f; //1 attack / 2 seconds
      
    

    private float[] birddmg = new float[2] { 1f, 5f };    
    private float[] humandmg = new float[2] { 5f, 15f };
    private float[] wolfdmg = new float[2] { 10f, 15f };
    private float[] beardmg = new float[2] { 10f, 30f };
    

    void Start () {        
        ps = GetComponent<PlayerScript>();
        hs = GetComponent<HealthScript>();
        pv = GetComponent<PhotonView>();
        stats = GetComponent<Stats>();
        animator = GetComponent<Animator>();
        ws = GetComponent<WeaponScript>();
        character = ps.character;      

        stats.strength = 10;
        stats.dexterity = 10;
        stats.constitution = 20;
        stats.intelligence = 35;
        stats.wisdom = 10;
        stats.charisma = 5;

        stats.damage = humandmg;        
        stats.critchance = 0.05f;
        stats.basecritchance = stats.critchance;
        stats.critbonus = 1.5f;                     
        stats.speed = new Vector2(0.8f, 0.8f);

        ws.shootingRate = 1f;
        ws.shotPrefab = ws.weapons[2]; //staff
        stats.InitializeResources();
                
        
    }

    void Update()
    {
        if (stats.isControllable)
        {
            if (!ps.casting)
            {
                Druid_Bindings();                
            }
        }
    }

    private void Druid_Bindings()
    {
        if (!ps.casting)
        {
            if (Input.GetKeyDown("y"))
            {
                pv.RPC("Shapeshift", PhotonTargets.AllBuffered, "bird", 25f);                
            }
            else if (Input.GetKeyDown("h"))
            {
                //shapehisft bear
                pv.RPC("Shapeshift", PhotonTargets.AllBuffered, "bear", 25f);                
            }
            else if (Input.GetKeyDown("j"))
            {                
                pv.RPC("Shapeshift", PhotonTargets.AllBuffered, "wolf", 25f);                
            }
            else if (Input.GetKeyDown("n"))
            {
                pv.RPC("Shapeshift", PhotonTargets.AllBuffered, "human", 25f);                
            }
            else if (Input.GetKeyDown("u"))
            {
                pv.RPC("Cast_Cure", PhotonTargets.AllBuffered, 20f, 2f, cureLightPrefab);
            }
            else if (Input.GetKeyDown("i"))
            {
                pv.RPC("Cast_Cure", PhotonTargets.AllBuffered, 20f, 2f, cureLinePrefab);
            }

        }
    }

    public void Druid_Attack(Vector3 direction)
    {
        float damage = stats.CalculateWeaponDamage();        

        if (ws.CanAttack)
            pv.RPC("Melee_Attack", PhotonTargets.All, ps.dir, damage);
    }    

    [RPC]
    private void Cast_Cure(float manaCost, float castTime, Transform prefab)
    {
        StartCoroutine(Cure_Light(manaCost, castTime, prefab));
    }
    private IEnumerator Cure_Light(float manaCost, float castTime, Transform prefab)
    {
        if (hs.UseMana(manaCost))
        {
            ps.Cast("CastFX", castTime);
            yield return new WaitForSeconds(castTime);
                        
            var shotTransform = Instantiate(prefab) as Transform;            
            
            if (shotTransform != null)
            {
                ShotScript shot = shotTransform.GetComponent<ShotScript>();
                shot.transform.position = transform.position;
                shot.heal = true;
                shot.SetID(ws.GetInstanceID());
                shot.damage = Random.Range(10f, 20f);
                
                  
                MoveScript move = shotTransform.GetComponent<MoveScript>();
                if (move != null)
                {
                    move.direction = ps.dir;
                }    
            }
            ps.PostCast();
        }        
    }

    

    [RPC]
    private void Shapeshift(string form, float manacost)
    {
        if (form != "human")
            StartCoroutine(Shapeshift_Form(form, manacost));
        else
            StartCoroutine(Shapeshift_Human());
    }

    private IEnumerator Shapeshift_Form(string form, float ManaCost)
    {
        //Can only shapeshift from human form
        if (animator.GetBool("human"))
        {
            if (hs.UseMana(ManaCost))
            {
                ps.Cast("ShapeshiftFX", 1f);
                yield return new WaitForSeconds(1f);
                ps.PostCast();
                animator.SetBool("human", false);

                if (form == "bird")
                {
                    curForm = "bird";
                    animator.SetBool("bird", true);                    
                    stats.UpdateStats(con: -5, speedmod: new Vector2(1f, 1f));
                    stats.damage = birddmg;
                    ws.shootingRate = birdAttackRate;
                    ws.shotPrefab = ws.weapons[1];
                    yield return new WaitForSeconds(birdDuration);
                    StartCoroutine(Shapeshift_Human());
                }
                else if (form == "bear")
                {
                    curForm = "bear";
                    animator.SetBool("bear", true);                    
                    stats.dmgReducedByHit += bearPerHit;
                    stats.PercentReduction += bearReduction;
                    stats.UpdateStats(con: 10, speedmod: -new Vector2(0.25f, 0.25f));
                    stats.damage = beardmg;
                    ws.shootingRate = bearAttackRate;
                    ws.shotPrefab = ws.weapons[0];

                }
                else if (form == "wolf")
                {
                    curForm = "wolf"; 
                    animator.SetBool("wolf", true);                    
                    stats.UpdateStats(con: 5);
                    stats.damage = wolfdmg;
                    ws.shootingRate = wolfAttackRate;
                    ws.shotPrefab = ws.weapons[1];
                }
            }

        }


    }
    private IEnumerator Shapeshift_Human()
    {        
        if (!animator.GetBool("human"))
        {
            if (animator.GetBool("bird"))
            {
                stats.UpdateStats(con: 5, speedmod: new Vector2(-1f, -1f));
            }
            if (animator.GetBool("bear"))
            {
                stats.UpdateStats(con: -10, speedmod: new Vector2(-0.25f, -0.25f));
                stats.dmgReducedByHit -= bearPerHit;
                stats.PercentReduction -= bearReduction; 
            }
            if (animator.GetBool("wolf"))
            {
                stats.UpdateStats(con: -5);
            }

            curForm = "human";               
            stats.damage = humandmg;
            ps.Cast("ShapeshiftFX", 1f);
            yield return new WaitForSeconds(1f);
            ShapeshiftHeal();
            ws.shootingRate = humanAttackRate;
            ws.shotPrefab = ws.weapons[2];
            animator.SetBool("bird", false);
            animator.SetBool("bear", false);
            animator.SetBool("wolf", false);
            animator.SetBool("human", true);
            ps.PostCast();
        }
    }
    public void ShapeshiftHeal()
    {
        //Standard 10, heals 13-20 health for 7 mana                
        bool success = hs.UseMana(10);

        if (success)
        {
            var amount = Random.Range(20f, 30f);

            if (pv.isMine)
            {
                pv.RPC("AddHealth", PhotonTargets.All, amount);
                pv.RPC("HealFX", PhotonTargets.All, 0.25f);
            }
        }
    }
   
}
