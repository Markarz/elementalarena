﻿using UnityEngine;
using System.Collections;

public class RangerPowers : MonoBehaviour {

    private Animator animator;
    private GenericPowers gp;
    private HealthScript hs;
    private PlayerScript ps;
    private PhotonView pv;
    private Stats stats;
    private WeaponScript ws;
    private FxManager fx; 
    private string character;    

    private Vector2 speed;
    private float speedMod;    
    
    private bool hasBarkskin = false;    
    private int selectedWeapon = 0;

    private float[] BowDmg = new float[2] { 10f, 15f };
    private float[] SpearDmg = new float[2] { 15f, 25f };
    private float[] GunDmg = new float[2] { 1f, 5f };

    private AudioClip shotAudio;
    public AudioClip bowAudio;

    private void Start()
    {
        ps = GetComponent<PlayerScript>();
        hs = GetComponent<HealthScript>();
        pv = GetComponent<PhotonView>();
        stats = GetComponent<Stats>();
        animator = GetComponent<Animator>();
        ws = GetComponent<WeaponScript>();
        gp = GetComponent<GenericPowers>();
        fx = GetComponent<FxManager>();

        character = ps.character;                
                
        stats.strength = 10;
        stats.dexterity = 10;
        stats.constitution = 20;
        stats.intelligence = 20;
        stats.wisdom = 5;
        stats.charisma = 5;        
        stats.speed = new Vector2(0.8f, 0.8f);

        stats.critchance = 0.05f;
        stats.basecritchance = stats.critchance;
        stats.InitializeResources();
        //default to the bow
        SwitchBow();
        shotAudio = bowAudio;
    }

    private void Update()
    {
        if (stats.isControllable)
        {            
            Ranger_Bindings();            
        }
    }

    private void Ranger_Bindings()
    {
        if (!ps.casting)
        {
            //Arrows
            if (Input.GetKeyDown("1"))
            {
                pv.RPC("SwitchWeapon", PhotonTargets.All, 0);
            }
            //Spears
            else if (Input.GetKeyDown("2"))
            {
                pv.RPC("SwitchWeapon", PhotonTargets.All, 1);
            }
            //Bullets
            else if (Input.GetKeyDown("3"))
            {
                pv.RPC("SwitchWeapon", PhotonTargets.All, 2);
            }
            else if (Input.GetKeyDown("y"))
            {
                //Damage reduced by 3 per hit and 30%
                pv.RPC("CastBarkskin", PhotonTargets.All);
            }
            else if (Input.GetKeyDown("h"))
            {
                pv.RPC("TriShot", PhotonTargets.AllBuffered, ps.dir, 15f, 0.1f, 0.5f);   
            }
            else if (Input.GetKeyDown("j"))
            {
                pv.RPC("MultiShot", PhotonTargets.AllBuffered, ps.dir, 15f, 5f);  
            }
            
        }
    }

    

    public void Ranger_Attack(Vector3 direction)
    {
        if (ws.CanAttack)
        {
            var dmg = stats.CalculateWeaponDamage();
            pv.RPC("Attack", PhotonTargets.All, ps.dir, dmg, shotAudio);                        
        }
        
    }    

    [RPC]
    private void SwitchWeapon(int slot)
    {
        selectedWeapon = slot; 
        ws.shotPrefab = ws.weapons[slot];        
        if (slot == 0)
        {
            SwitchBow();            
        }
        if (slot == 1)
        {
            SwitchSpear();
        }
        if (slot == 2)
        {
            SwitchGun();
        }
    }
    

    private void SwitchBow()
    {
        Debug.Log("switch bow: " + fx.bowAudio); 
        stats.damage = BowDmg;
        ws.shootingRate = 0.75f;
        shotAudio = fx.bowAudio;
    }
    private void SwitchSpear()
    {
        //spear
        stats.damage = SpearDmg;
        ws.shootingRate = 1.3f;
        shotAudio = fx.spearAudio;
    }
    private void SwitchGun()
    {
        //bullets
        stats.damage = GunDmg;
        ws.shootingRate = 0.15f;
        shotAudio = fx.bulletAudio;
    }

    [RPC]
    public IEnumerator MultiShot(Vector3 Direction, float ManaCost, float maxShots)
    {        
        bool success = hs.UseMana(ManaCost);
        if (success)
        {
            stats.isControllable = false; 
            pv.RPC("EffectFX", PhotonTargets.All, 0.8f);

            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < maxShots; i++)
            {
                var dmg = stats.CalculateDamage(BowDmg[0], BowDmg[1]);
                var shotTransform = Instantiate(ws.weapons[0]) as Transform;
                ShotScript ss = shotTransform.GetComponent<ShotScript>();
                ss.damage = dmg;
                ss.SetID(ws.GetInstanceID());

                shotTransform.position = transform.position;
                shotTransform.Rotate(Direction);
                shotTransform.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);                
                // Make the weapon shot always towards it
                MoveScript move = shotTransform.GetComponent<MoveScript>();
                if (move != null)
                {                    
                    move.direction = Direction;
                }

                GetComponent<AudioSource>().PlayOneShot(fx.bowAudio);

                yield return new WaitForSeconds(0.15f);
            }
            stats.isControllable = true; 
        }
    }
            

    [RPC]
    public IEnumerator TriShot(Vector3 Direction, float ManaCost, float Spread, float CastTime)
    {                      
        //can only hit the same target with one arrow
        //direction is locked in at activation time, not completion time
        //0.2f is a tight spread, 0.5f is +-45 degrees.
        var success = false;
        var shotPrefab = ws.weapons[0]; //arrows only
        var topShot = Direction;
        var bottomShot = Direction;

        if (Direction.x < 0)
        {
            //if facing left
            topShot.x = -0.5f;
            topShot.y = Spread;
            bottomShot.x = -0.5f;
            bottomShot.y = -Spread;
        }
        if (Direction.x > 0)
        {
            topShot.x = 0.5f;
            topShot.y = Spread;
            bottomShot.x = 0.5f;
            bottomShot.y = -Spread;
        }
        if (Direction.y > 0)
        {
            topShot.x = -Spread;
            topShot.y = 0.5f;
            bottomShot.x = Spread;
            bottomShot.y = 0.5f;
        }
        if (Direction.y < 0)
        {
            topShot.x = -Spread;
            topShot.y = -0.5f;
            bottomShot.x = Spread;
            bottomShot.y = -0.5f;
        }        

        if (ManaCost > 0)
            success = hs.UseMana(ManaCost);

        if(success)
        {
            ps.casting = true;
            stats.isControllable = false; 
            pv.RPC("EffectFX", PhotonTargets.All, CastTime);
            yield return new WaitForSeconds(CastTime);

            //top shot
            var dmg = stats.CalculateDamage(BowDmg[0], BowDmg[1]);
            var shotTransform2 = Instantiate(shotPrefab) as Transform;
            ShotScript ss2 = shotTransform2.GetComponent<ShotScript>();
            ss2.damage = dmg;
            ss2.SetID(ws.GetInstanceID());
            shotTransform2.position = transform.position;
            shotTransform2.Rotate(topShot);
            shotTransform2.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);
            shotTransform2.GetComponent<ShotScript>().damage = dmg;            
            MoveScript move2 = shotTransform2.GetComponent<MoveScript>();
            move2.direction = topShot;
            move2.speed = move2.speed * 2f;
            
            //middle shot
            var shotTransform = Instantiate(shotPrefab) as Transform;
            ShotScript ss = shotTransform.GetComponent<ShotScript>();
            ss.damage = dmg;
            ss.SetID(ws.GetInstanceID());
            shotTransform.position = transform.position;
            shotTransform.Rotate(Direction);
            shotTransform.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);
            shotTransform.GetComponent<ShotScript>().damage = dmg;            
            MoveScript move = shotTransform.GetComponent<MoveScript>();
            move.direction = Direction;
            
            //bottom shot
            var shotTransform3 = Instantiate(shotPrefab) as Transform;
            ShotScript ss3 = shotTransform3.GetComponent<ShotScript>();
            ss3.damage = dmg;
            ss3.SetID(ws.GetInstanceID());
            shotTransform3.position = transform.position;
            shotTransform3.Rotate(bottomShot);
            shotTransform3.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);
            shotTransform3.GetComponent<ShotScript>().damage = dmg;            
            MoveScript move3 = shotTransform3.GetComponent<MoveScript>();
            move3.direction = bottomShot;
            move3.speed = move3.speed * 2f;
            GetComponent<AudioSource>().PlayOneShot(fx.bowAudio);
            GetComponent<AudioSource>().PlayOneShot(fx.bowAudio);
            GetComponent<AudioSource>().PlayOneShot(fx.bowAudio);
            ps.casting = false;
            stats.isControllable = true; 
        }
                    
    }

    [RPC]
    public void CastBarkskin()
    {
        StartCoroutine(Barkskin(20f, 3f, 2f, 30f));  
    }
    //All power modifies are in related to floats, so 10% = 1.1f
    private IEnumerator Barkskin(float ManaCost, float Power, float CastTime, float Duration)
    {
        if (!hasBarkskin)
        {
            bool success = hs.UseMana(ManaCost);

            if (success)
            {
                ps.Cast("CastFX", CastTime);
                yield return new WaitForSeconds(CastTime);                
                stats.PercentReduction += 0.3f;
                hasBarkskin = true;
            }
            GetComponent<AudioSource>().PlayOneShot(fx.fortifyClip);
            ps.PostCast();
            pv.RPC("FortifyFX", PhotonTargets.All, Duration);
            //Buffed for 30 seconds.
            yield return new WaitForSeconds(Duration);

            hasBarkskin = false;                        
            stats.PercentReduction -= 0.3f;
        }
    }       
}
