﻿using UnityEngine;
using System.Collections;

public class WizardSpells : MonoBehaviour
{

    private Animator animator;
    private GenericPowers gp;
    private HealthScript hs;
    private PlayerScript ps;
    private PhotonView pv;
    private Stats stats;
    private WeaponScript ws;
    private FxManager fx;
    private TeamMember team;

    public Transform MeteorPrefab;    
    public Transform castPrefab;    
    public Transform ConeOfColdPrefab;    

    private Transform targetCircle;

    private string character;
    private float hp;
    private float hpMax;
    private float hpMod;
    private float hpMaxMod;

    private float mana;
    private float manaMax;
    private float manaMod;
    private float manaMaxMod;    

    private float frequency = 0.1f;
    private float timer;
    private float distance = 0f;    

    private void Start()
    {
        ps = GetComponent<PlayerScript>();
        hs = GetComponent<HealthScript>();
        pv = GetComponent<PhotonView>();
        stats = GetComponent<Stats>();
        animator = GetComponent<Animator>();
        ws = GetComponent<WeaponScript>();
        gp = GetComponent<GenericPowers>();
        fx = GetComponent<FxManager>();                
        team = GetComponent<TeamMember>();

        character = ps.character;
        hp = stats.hp;
        hpMax = stats.hpmax;        
        mana = stats.mana;
        manaMax = stats.manamax;

        stats.damage = new float[] { 10f, 15f };        
        stats.critchance = 0.1f;
        stats.basecritchance = stats.critchance;                
        ws.shootingRate = 1f;        
        
        stats.strength = 10;
        stats.dexterity = 10;
        stats.constitution = 20;
        stats.intelligence = 50;
        stats.wisdom = 10;
        stats.charisma = 5;
        stats.speed = new Vector2(1.0f, 1.0f);
        stats.basespeed = new Vector2(1.0f, 1.0f); 
        stats.InitializeResources();        		
    }

    private void Update()
    {
        if (stats.isControllable)
        {            
            Wizard_Bindings();
        }
    }

    
    private void Wizard_Bindings()
    {
        if (!ps.casting)
        {
            if (Input.GetKeyDown("y"))
            {
                StartCoroutine(Blink(25f, 1f, 1.2f, transform));
            }
            else if (Input.GetKeyDown("u"))
            {
                Heal();
            }
            else if (Input.GetKeyDown("i"))
            {
                GreaterHeal();
            }
            else if (Input.GetKeyDown("n"))
            {
                StartCoroutine(Lifetap(0f, 25f, 0.5f));
            }
            else if (Input.GetKeyDown("h"))
            {
                //mana, cast time, speed of projectile
                pv.RPC("CastFireball", PhotonTargets.All, 10f, 0.3f, 3f);
            }

            if (Input.GetKeyDown("j"))
            {
                //instantiate a local casting circle that moves at the same rate as the distance spreads out    
                //localCastingCircle = LocalCastingCircle();
                pv.RPC("TargetCircle", PhotonTargets.All);
            }

            if (Input.GetKey("j"))
            {                              
                //start timer
                timer += Time.deltaTime;
                if (timer > frequency)
                {
                    distance += 0.2f;
                    timer = 0f;
                    var newPosition = transform.position + (ps.dir * distance);
                    pv.RPC("MoveTargetCircle", PhotonTargets.All, newPosition);                 
                }    
            }
            if (Input.GetKeyUp("j"))
            {
                //2f max distance
                if (distance > 3f)
                    distance = 3f;

                pv.RPC("CastMeteor", PhotonTargets.All, 35f, 1f, distance);
                distance = 0f;
                timer = 0f;
                //Destroy(localCastingCircle.gameObject);
            }

            if (Input.GetKey("k"))
            {
                pv.RPC("CastConeOfCold", PhotonTargets.All, 20f, 1f); 
            }
        }
    }

    public void Wizard_Attack()
    {
        float dmg = stats.CalculateWeaponDamage();
        
        if (ws.CanAttack)
            pv.RPC("Melee_Attack", PhotonTargets.All, ps.dir, dmg);
    }


    [RPC]
    public void MoveTargetCircle(Vector3 newPosition)
    {        
        targetCircle.position = newPosition;
    }

    [RPC]
    public void TargetCircle()
    {        
        var shotTransform = Instantiate(castPrefab) as Transform;
        shotTransform.position = transform.position;

        targetCircle = shotTransform;
    }

    private void Heal()
    {
        //mana, power, min, max, cast time
        StartCoroutine(gp.Heal(25f, 15f, 30f, 0.5f));
    }

    private void GreaterHeal()
    {
        //mana, power, min, max, cast time
        StartCoroutine(gp.Heal(25f, 40f, 85f, 2f));
    }

    //Add Stoneskin spell. X hits of any size, no duration, long cast time. 

    [RPC]
    public void CastMeteor(float ManaCost, float CastTime, float MaxRange)
    {        
        if(pv.isMine)
            StartCoroutine(Meteor(transform, ManaCost, CastTime, MaxRange));        
    }

    private IEnumerator MeteorCircle(float CastTime, Vector3 position)
    {
        pv.RPC("MeteorCastFX", PhotonTargets.All, CastTime, position);
        yield return new WaitForSeconds(CastTime);
    }

    private IEnumerator Meteor(Transform playerPos, float ManaCost, float CastTime, float MaxRange)
    {       
        bool success = hs.UseMana(ManaCost);
                
        var newx = playerPos.position.x + (ps.dir.x * MaxRange);
        var newy = playerPos.position.y + (ps.dir.y * MaxRange);
        var position = gp.CheckBounds(new Vector3(newx, newy, 0));
        
        if (position == Vector3.zero)
            position = new Vector3(newx, newy, 0);

        if (success)
        {            
            ps.Cast("CastFX", CastTime);
            //StartCoroutine(MeteorCircle(CastTime, position));
            yield return new WaitForSeconds(CastTime);

            //var shotTransform = Instantiate(MeteorPrefab) as Transform;            
            var shotTransform = PhotonNetwork.Instantiate("Meteor", position, Quaternion.identity, 0);
            //shotTransform.position = position;            
            shotTransform.GetComponent<TeamMember>().SetTeamID(-2);

            var dmg = stats.CalculateDamage(25f, 50f);
            ShotScript shot = shotTransform.GetComponent<ShotScript>();
            shot.damage = dmg;
            shot.networked = true; 
            ps.PostCast();

            yield return new WaitForSeconds(1f);
            GetComponent<AudioSource>().PlayOneShot(fx.MeteorClip);
        }        
    }

    [RPC]
    public void CastConeOfCold(float ManaCost, float CastTime)
    {
        if (pv.isMine)
            StartCoroutine(ConeOfCold(ManaCost, CastTime));
    }
    private IEnumerator ConeOfCold(float ManaCost, float CastTime)
    { 
        if (hs.UseMana(ManaCost))
        {
            var dmg = stats.CalculateDamage(20f, 30f);

            ps.Cast("CastFX", CastTime);
            yield return new WaitForSeconds(CastTime);            
            var shot = PhotonNetwork.Instantiate("ConeFX", transform.position, Quaternion.Euler(ps.dir), 0);                        
            //Transform shot = Instantiate(ConeOfColdPrefab, transform.position, Quaternion.Euler(ps.dir)) as Transform;            
            TeamMember shotTeam = shot.GetComponent<TeamMember>();
            shotTeam.SetTeamID(team.teamID);                        
            
            ShotScript ss = shot.GetComponent<ShotScript>();
            ss.damage = dmg;
            ss.SetID(ws.GetInstanceID());
            ss.isConeOfCold = true;
            //ss.isNetworked = true;                                     
            
            ps.PostCast();
        }
    }

    [RPC]
    public void CastFireball(float ManaCost, float CastTime, float Speed)
    {
        //if (pv.isMine)
            StartCoroutine(Fireball(ManaCost, CastTime, Speed));
    }

    private IEnumerator Fireball(float ManaCost, float CastTime, float speed)
    {
        if (fx.FireballPrefab == null)
        {
            Debug.Log("Missing fireball prefab on wizard.");
        }

        if (hs.UseMana(ManaCost))
        {
            var dmg = stats.CalculateDamage(25f, 35f);

            ps.Cast("CastFX", CastTime);
            yield return new WaitForSeconds(CastTime);

            Transform shotTransform = Instantiate(fx.FireballPrefab) as Transform;
            
            ShotScript ss = shotTransform.GetComponent<ShotScript>();
            ss.damage = dmg;
            ss.SetID(ws.GetInstanceID());

            shotTransform.position = transform.position;
            shotTransform.Rotate(ps.dir);
            shotTransform.GetComponent<TeamMember>().SetTeamID(transform.GetComponent<TeamMember>().teamID);                        
            
            MoveScript move = shotTransform.GetComponent<MoveScript>();
            move.direction = ps.dir;

            GetComponent<AudioSource>().PlayOneShot(fx.fireballClip);
            ps.PostCast();
        }
    }
   

    public IEnumerator Blink(float ManaCost, float CastTime, float MaxBlinkDistance, Transform player)
    {
        bool success = hs.UseMana(ManaCost);

        if (success)
        {
            ps.Cast("BlinkFX", 1.5f);            
            yield return new WaitForSeconds(CastTime);            
            if(fx.blinkClip != null)
                GetComponent<AudioSource>().PlayOneShot(fx.blinkClip);

            var newx = player.position.x + (ps.dir.x * MaxBlinkDistance);
            var newy = player.position.y + (ps.dir.y * MaxBlinkDistance);
            var newpos = gp.CheckBounds(new Vector3(newx, newy, -1));

            if (newpos == Vector3.zero)
                newpos = new Vector3(newx, newy, -1);

            player.position = newpos;
            ps.PostCast();
        }
        else
        {
            Debug.Log("Not enough mana.");
        }        
    }

    //public IEnumerator Haste(float ManaCost, float Power, float CastTime, float Duration)
    //{
    //    //No casting while hasted.
    //    bool success = hs.UseMana(ManaCost);

    //    if (success)
    //    {
    //        ps.Cast("CastFX", CastTime);
    //        yield return new WaitForSeconds(CastTime);
    //        stats.speed = speed * Power;

    //        yield return new WaitForSeconds(Duration);
    //        ps.PostCast();
    //    }        
    //}
    
    public IEnumerator Lifetap(float ManaCost, float Power, float CastTime)
    {
        ps.Cast("CastFX", CastTime);
        yield return new WaitForSeconds(CastTime);

        if (pv.isMine)
        {
            pv.RPC("Damage", PhotonTargets.All, Power);
            pv.RPC("LifetapFX", PhotonTargets.All);
            hs.AddMana(Power * 2f);
            ps.PostCast();
        }        
    }

}
