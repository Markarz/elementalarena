﻿using UnityEngine;
using System.Collections;

public class KnightPowers : MonoBehaviour {

    private Animator animator;
    private GenericPowers gp;
    private HealthScript hs;
    private PlayerScript ps;
    private PhotonView pv;
    private Stats stats;
    private WeaponScript ws;
    private TeamMember team;
    private Utils utils;
    private FxManager fx; 
    private float[] SwordDmg = new float[2] { 10f, 15f };
        
    private string character;
    
    private bool isFortified = false;
    private bool isCharging = false;
    private bool isSprinting = false;

    private bool canCharge = true;
    private bool canHeal = true;
    private bool canSprint = true; 

    private void Start()
    {
        ps = GetComponent<PlayerScript>();
        hs = GetComponent<HealthScript>();
        pv = GetComponent<PhotonView>();
        stats = GetComponent<Stats>();
        animator = GetComponent<Animator>();
        ws = GetComponent<WeaponScript>();
        gp = GetComponent<GenericPowers>();
        team = GetComponent<TeamMember>();
        utils = GetComponent<Utils>();
        fx = GetComponent<FxManager>();

        character = ps.character;        
        ws.shootingRate = 0.35f;

        stats.strength = 10;
        stats.dexterity = 10;
        stats.constitution = 30;
        stats.intelligence = 20;
        stats.wisdom = 10;
        stats.charisma = 5;
        stats.InitializeResources();

        stats.damage = SwordDmg;        
        stats.critchance = 0.1f;        
        stats.critbonus = 1.5f;        
        stats.speed = new Vector2(1f,1f);  
    }

    private void Update()
    {
        if (stats.isControllable)
        {            
            Knight_Bindings();
        }
    }

    private void Knight_Bindings()
    {
        if (!ps.casting)
        {
            if (Input.GetKeyDown("y"))
            {
                if(canSprint)
                    StartCoroutine(Sprint(30f, 1.5f, 0.5f, 3f, 20f));
            }
            else if (Input.GetKeyDown("u"))
            {
                if(canHeal)
                    StartCoroutine(Heal(6f)); 
            }
            else if (Input.GetKeyDown("i"))
            {                
                StartCoroutine(Fortify(25f, 1.5f, 2.5f, 25f));
            }

            if (Input.GetKeyDown("h"))
            {
                if (canCharge)
                {
                    StartCoroutine(Charge(25f, 3f));
                }
            }
        }
    }

    public void Knight_Attack(Vector3 direction)
    {
        var dmg = stats.CalculateWeaponDamage();

        if (ws.CanAttack)
            pv.RPC("Melee_Attack", PhotonTargets.All, ps.dir, dmg);
    }    

    private IEnumerator Heal(float Cooldown)
    {
        canHeal = false; 
        StartCoroutine(gp.Heal(15f, 20f, 30f, 0.5f));
        yield return new WaitForSeconds(Cooldown);
        canHeal = true; 
    }

    public GameObject FindClosestTarget()
    {        
        var players = GameObject.FindGameObjectsWithTag("Player");
        var enemies = GameObject.FindGameObjectsWithTag("enemy");
        var distance = 0f;
        GameObject target = gameObject;

        if (players != null && players.Length > 1)
        {
            foreach (GameObject player in players)
            {                
                Vector3 distanceVector = player.transform.position - transform.position;
                if (distanceVector.sqrMagnitude > distance)
                {
                    distance = distanceVector.sqrMagnitude;
                    target = player;
                    return target;
                }
            }            
        }

        if(target == gameObject && enemies.Length > 0)
        {
            foreach(GameObject enemy in enemies)
            {
                Vector3 distanceVector = enemy.transform.position - transform.position;
                    
                if (distanceVector.sqrMagnitude > distance)
                {
                    distance = distanceVector.sqrMagnitude;
                    target = enemy;                    
                }
            }
        }        

        return target;
    }    
    IEnumerator Charge(float ManaCost, float MaxDistance)
    {        
        canCharge = false; 
        GameObject target = FindClosestTarget();
        Stats targetStats = target.GetComponent<Stats>();

        Vector3 dir = target.transform.position - transform.position;
        dir.Normalize();

        Debug.Log("In Charge"); 
        RaycastHit2D hit = Physics2D.Raycast(transform.position + (ps.dir * 0.15f), dir, MaxDistance);
        Debug.DrawRay(transform.position + (ps.dir*0.15f), dir, Color.red, 2f);

        Debug.Log("Hit: " + hit.transform); 
        if (hit.transform == target.transform)
        {
            if (hs.UseMana(ManaCost))
            {
                ps.Cast("CastFX", 1f); 
                yield return new WaitForSeconds(1f);
                ps.PostCast();

                var distance = Vector3.Distance(target.transform.position, transform.position);
                Vector3 fxPos = target.transform.position;
                fxPos.y += 0.2f;
                pv.RPC("StunnedFX", PhotonTargets.All, fxPos, 2f);
                Debug.Log("successful hit"); 
                if (distance < MaxDistance && distance > 0 && target != gameObject)
                {
                    iTween.MoveTo(gameObject, iTween.Hash("position", hit.transform.position * 1.02f, "easetype", "linear", "speed", 5f));
                    
                    //stun for 2 seconds
                    targetStats.isControllable = false; 
                    yield return new WaitForSeconds(2f);
                    targetStats.isControllable = true; 
                }
                else
                {
                    Debug.Log("No Targets");
                }
                yield return new WaitForSeconds(15f); 
                canCharge = true; 
            }
        }



    }

    //public void Charge(float MaxDistance)
    //{
    //    var newx = transform.position.x + (ps.dir.x * 1.5f);
    //    var newy = transform.position.y + (ps.dir.y * 1.5f);
    //    var newpos = new Vector3(newx, newy, -1);

    //    GameObject enemy = utils.FindClosestEnemyPlayer(team.teamID);                                



    //    Vector3 distance = (enemy.transform.position - transform.position);
    //    Debug.Log("distance: " + distance.sqrMagnitude); 

    //    if (distance.sqrMagnitude < MaxDistance)
    //    {

    //    }

    //    RaycastHit2D hit = Physics2D.Raycast(transform.position + (ps.dir * 0.3f), ps.dir, 1f);                
    //    Debug.Log("ps: " + gameObject.transform.position);
    //    Debug.Log("hit " + hit.transform.position);
        
    //    iTween.MoveTo(gameObject, hit.transform.position, 1f);         
        
    //}
    
    
    public IEnumerator Sprint(float ManaCost, float Power, float CastTime, float Duration, float Cooldown)
    {
        if (!isSprinting)
        {
            bool success = hs.UseMana(ManaCost);

            if (success)
            {
                isSprinting = true;
                canSprint = false; 
                ps.Cast("CastFX", CastTime);
                yield return new WaitForSeconds(CastTime);
                stats.UpdateStats(speedmod: new Vector2(0.5f, 0.5f));
                GetComponent<AudioSource>().PlayOneShot(fx.sprintClip);

                ps.PostCast();

                yield return new WaitForSeconds(Duration);
                stats.UpdateStats(speedmod: new Vector2(-0.5f, -0.5f));
                isSprinting = false;
                yield return new WaitForSeconds(Cooldown);
                canSprint = true; 
            }
        }
    }


    private IEnumerator Fortify(float ManaCost, float Power, float CastTime, float Duration)
    {
        if (!isFortified)
        {
            bool success = hs.UseMana(ManaCost);

            if (success)
            {
                ps.Cast("CastFX", CastTime);
                yield return new WaitForSeconds(CastTime);
                GetComponent<AudioSource>().PlayOneShot(fx.fortifyClip);
                
                stats.UpdateStats(con: 5);
                stats.dmgReducedByHit = 5;                
                isFortified = true;
                ps.PostCast();

                pv.RPC("FortifyFX", PhotonTargets.All, Duration);
                //Buffed for 30 seconds.
                yield return new WaitForSeconds(Duration);

                isFortified = false;
                stats.UpdateStats(con: -5);
                stats.dmgReducedByHit = 0; 
            }                        
        }
    }    
}
