﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour
{
    public float damage;
    public float[] range { get; set; }
    public bool isEnemyShot { get; set; }    
    public float timetolive;
    public bool selfDestruct;
    public float id { get; set; }
    public bool heal = false;
    public bool isConeOfCold;
    public bool networked; 
    public Stack hitList = new Stack();    
    	
	void Start()
	{        
        // 2 - Limited time to live to avoid any leak
        Destroy(gameObject, timetolive); // 20sec
        if (networked)
        {            
            StartCoroutine(networkDestroy());
        }
        else
        {            
            Destroy(gameObject, timetolive);
        }
	}

    IEnumerator networkDestroy()
    {
        yield return new WaitForSeconds(timetolive);
        PhotonNetwork.Destroy(gameObject);
    }

    [RPC]
    public void SetID(int shotid)
    {
        id = shotid;
    }

    //Have someone spot check this code. 
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        TeamMember tm = otherCollider.GetComponent<TeamMember>();
        TeamMember myTm = GetComponent<TeamMember>();

        if (tm == null || myTm == null || tm.teamID == 0 || myTm.teamID == 0 || tm.teamID != myTm.teamID)                                    
        {
            var enemy = otherCollider.gameObject;
           
             if (hitList.Contains(enemy.GetInstanceID()))
             {
                if (hitList.Count > 15)
                {
                    hitList.Pop();
                }

                return;
             }

            if (hitList.Count == 0 || !hitList.Contains(enemy.GetInstanceID()))
            {
                //Always push the first entry
                hitList.Push(enemy.GetInstanceID());                
                //Handle collisions
                PhotonView pv = otherCollider.GetComponent<PhotonView>();

                if (isConeOfCold && PhotonNetwork.isMasterClient)
                {
                    var hs = otherCollider.GetComponent<HealthScript>();
                    if (hs != null)
                    {
                        var speedReduction = 0.1f; 
                        var otherSpeed = otherCollider.GetComponent<Stats>().speed;
                        var speedmod = otherSpeed * speedReduction;
                        //var speedmod = new Vector2(otherSpeed.x * speedReduction, otherSpeed.y * speedReduction);                        
                        StartCoroutine(hs.FreezeEffect(otherCollider, -speedmod, 5f));   
                    }                    
                }    
            }
                                           
         }    
        
                            
    }    
        
}