﻿using UnityEngine;
using System.Collections;

public class HealthScriptObject : MonoBehaviour {

    public float hp = 1000;
    public float hpmax = 1000;    
    public bool isDestructable = false; //this will be for objects that need destroyed tiled
    public GameObject brokenwallPrefab;
    private PhotonView view;
    private FxManager fxManager;
    //This hit list prevents a target from being hit by area effect more than one time 
    //before the object is destroyed.
    public Stack hitList = new Stack();

	//Put this script on the "wall" layer of a tilemap to give the walls health and collisions.
	void Start ()
	{
        fxManager = GameObject.FindObjectOfType<FxManager>();
        transform.GetComponent<PhotonView>();        
	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        view = transform.GetComponent<PhotonView>();
        if (view == null)
        {
            Debug.Log("No photon view on this object.");
        }
        else
        {
            ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript>();
            
            if (shot != null)
            {
                if (shot.damage > 0)
                {
                    if (hitList.Count == 0)
                    {
                        hitList.Push(shot.gameObject);
                        view.RPC("DamageObject", PhotonTargets.AllBuffered, shot.damage);
                    }
                    else if (!hitList.Contains(shot.gameObject))
                    {
                        if (hitList.Count > 15)
                            hitList.Pop();
                        hitList.Push(shot.gameObject);
                        view.RPC("DamageObject", PhotonTargets.AllBuffered, shot.damage);
                    }
                    
                    if (!shot.selfDestruct)
                    {
                        if (shot.networked)
                        {
                            Debug.Log("netwrok destroy");
                            PhotonNetwork.Destroy(shot.gameObject);
                        }
                        else
                        {
                            Debug.Log("normal destroy");
                            Destroy(shot.gameObject);
                        } 
                    }
                   
                }
            }    
        }
        
    }

    [RPC]
    public void DamageObject(float amount)
    {
        hp -= amount;
        if (hp <= 0)
        {
            StartCoroutine(Die());
        }
    }
        
    private IEnumerator Die()
    {        
        hp = 0;
        enabled = false;
        fxManager.GetComponent<PhotonView>().RPC("LayTemporaryBrokenWall", PhotonTargets.All, gameObject.GetComponent<BoxCollider2D>().size, gameObject.transform.position, 5f);

        BoxCollider2D box = GetComponent<BoxCollider2D>();
        box.enabled = false; 
        yield return new WaitForSeconds(5f);
        box.enabled = true; 
        HealthScriptObject hso = GetComponent<HealthScriptObject>();
        hso.hp = hso.hpmax;        
        
    }

    private void InstantiateWalls()
    {
        foreach (Transform child in transform)
        {
            var childscript = child.gameObject.AddComponent<HealthScriptObject>();
            childscript.hp = hp;
            childscript.hpmax = hpmax;
            childscript.isDestructable = true;
        } 
    }

    public void Network_InstantiateWalls(int id)
    {        
        //PhotonNetwork.Instantiate("walls", transform.position, transform.rotation, 1);                
       
        foreach (Transform child in transform)
        {
            HealthScriptObject childscript = child.gameObject.AddComponent<HealthScriptObject>();
            childscript.hp = hp;
            childscript.hpmax = hpmax;
            childscript.isDestructable = true;
            childscript.brokenwallPrefab = brokenwallPrefab;

            PhotonView view = child.gameObject.AddComponent<PhotonView>();
            view.instantiationId = id;
            view.viewID = id;
            id++;

            FxManager brokenwallfx = child.gameObject.AddComponent<FxManager>();
            brokenwallfx.brokenwallPrefab = brokenwallPrefab;

        }
    }
}
