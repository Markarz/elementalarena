﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour {

    public bool connecting = false;
    NetworkManager nm;
    
	// Use this for initialization
	void Start () {
        nm = GetComponent<NetworkManager>();
	}

    void ChooseTeam()
    {        
        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Red Team"))
        {
            nm.teamID = 1;
            nm.hasPickedTeam = true;
        }

        if (GUILayout.Button("Green Team"))
        {
            nm.teamID = 2;
            nm.hasPickedTeam = true;
        }

        if (GUILayout.Button("Random"))
        {            
            nm.teamID = (Random.Range(1, 3));
            nm.hasPickedTeam = true;
        }

        if (GUILayout.Button("Renegade"))
        {
            nm.teamID = 3;
            nm.hasPickedTeam = true; 
        }        

        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }
    void ChooseCharacter()
    {        
        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Wizard"))
        {
            nm.character = "Red_Wizard";
            nm.hasPickedCharacter = true;
            nm.SpawnMyPlayer(nm.teamID);
        }

        if (GUILayout.Button("Knight"))
        {
            nm.character = "Knight";
            nm.hasPickedCharacter = true;
            nm.SpawnMyPlayer(nm.teamID);
        }

        if (GUILayout.Button("Druid"))
        {
            nm.character = "Druid";
            nm.hasPickedCharacter = true;
            nm.SpawnMyPlayer(nm.teamID);
        }

        if (GUILayout.Button("Ranger"))
        {
            nm.character = "Ranger";
            nm.hasPickedCharacter = true;
            nm.SpawnMyPlayer(nm.teamID);
        }

        if (GUILayout.Button("Marine"))
        {
            nm.character = "Marine";
            nm.hasPickedCharacter = true;
            nm.SpawnMyPlayer(nm.teamID);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }
    
    

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());              

        if (PhotonNetwork.connected == false && connecting == false)
        {
            // We have not yet connected, so ask the player for online vs offline mode.
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Username: ");            
            PhotonNetwork.player.name = GUILayout.TextField(PhotonNetwork.player.name);
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Offline"))
            {
                connecting = true;
                PhotonNetwork.offlineMode = true;
                nm.OnJoinedLobby();
            }

            if (GUILayout.Button("Online"))
            {
                connecting = true;
                nm.Connect();
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        if (PhotonNetwork.connected && connecting == false)
        {            
            if (!nm.hasPickedTeam || !nm.hasPickedCharacter)
            {                
                // Player has not yet selected a team or character
                if (!nm.hasPickedTeam)
                    ChooseTeam();
                else if (!nm.hasPickedCharacter)
                    ChooseCharacter();
            }
                                        
        }       
    }
}







