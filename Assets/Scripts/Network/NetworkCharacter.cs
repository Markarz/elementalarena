using UnityEngine;

public class NetworkCharacter : Photon.MonoBehaviour
{
    private Vector3 correctPlayerPos = Vector3.zero; // We lerp towards this
    private Vector3 correctPlayerScale = Vector3.zero; // We lerp towards this
   // private CameraFollow cameraScript;
    private HealthScript healthScript; 

    void Awake()
    {
        //cameraScript = Camera.main.GetComponent<CameraFollow>();
        healthScript = GetComponent<HealthScript>();        

        //if (photonView.isMine)
        //{
        //    //MINE: local player, simply enable the local scripts
        //   // cameraScript.enabled = true;
        //    healthScript.isEnemy = false;
        //}
        //else
        //{
        //   // cameraScript.enabled = false;
        //    healthScript.isEnemy = true;
        // }

        gameObject.name = gameObject.name + photonView.viewID;
    }   

    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, correctPlayerPos, 0.1f);                  
            //transform.position = correctPlayerPos;
            transform.localScale = correctPlayerScale;            
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {                
        if (stream.isWriting)
        {
            // We own this player: send the others our data            
            stream.SendNext(transform.position);
            stream.SendNext(transform.localScale);            
        }
        else
        {
            // Network player, receive data
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerScale = (Vector3) stream.ReceiveNext();
        }
    }
}
