﻿using UnityEngine;

public class RandomMatchmaker : Photon.MonoBehaviour
{
	private PhotonView myPhotonView;
    private int id = 0; 
	// Use this for initialization
	void Start()
	{
        //PhotonNetwork.offlineMode = true;
        //PhotonNetwork.CreateRoom("Offline Room");

        PhotonNetwork.ConnectUsingSettings("0.1");
        HealthScriptObject walls = GameObject.FindWithTag("walls").GetComponent<HealthScriptObject>();
        //walls.Network_InstantiateWalls(10000);

	}
	
	void OnJoinedLobby()
	{
		Debug.Log("JoinRandom");
		PhotonNetwork.JoinRandomRoom();
	}
	
	void OnPhotonRandomJoinFailed()
	{
	    Debug.Log("Joining random game failed. Creating room.");
		bool create = PhotonNetwork.CreateRoom(null);
	    if (!create)
	        Debug.Log("Failed to create a room. The network may be down.");
	    else
	        Debug.Log("Created a room.");
	}


    void OnJoinedRoom()
    {
        Debug.Log("Joined room.");
        id++;
        SpawnMyPlayer(id);        
    }

    void SpawnMyPlayer(int TeamID)
    {        
        //Vector3 randomStart = new Vector3(Random.Range(1, 11), Random.Range(-1, -11), 0);	    
        Vector3 randomStart = new Vector3(6.7f, -3.6f, 0);

        GameObject player = PhotonNetwork.Instantiate("Red_Wizard", randomStart, Quaternion.identity, 0);
        Stats stats = player.GetComponent<Stats>();
        stats.isControllable = true;        

        Camera camera = Camera.main;
        camera.enabled = true;

        HealthScript hpscript = player.GetComponent<HealthScript>();
        hpscript.enabled = true;

        player.GetComponent<PhotonView>().RPC("SetTeamID", PhotonTargets.AllBuffered, TeamID);

        //CameraFollow cfollower = camera.GetComponent<CameraFollow>();
        //cfollower.target = player.transform;
        
        
    }
	
    //void OnGUI()
    //{
    //    GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		
    //    if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
    //    {
    //        bool shoutMarco = GameLogic.playerWhoIsIt == PhotonNetwork.player.ID;
			
    //        if (shoutMarco && GUILayout.Button("Marco!"))
    //        {
    //            myPhotonView.RPC("Marco", PhotonTargets.All);
    //        }
    //        if (!shoutMarco && GUILayout.Button("Polo!"))
    //        {
    //            myPhotonView.RPC("Polo", PhotonTargets.All);
    //        }
    //    }
	//}
}
