﻿using UnityEngine;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour
{

    public GameObject standbyCamera;    
    SpawnSpot[] spawnSpots;
    LoadingScreen ls;
    public bool offlineMode = false;

    public string character = "Knight";
    public bool hasPickedCharacter = false;

    public List<string> chatMessages;
    int maxChatMessages = 10;

    public float respawnTimer = 0;
    public bool startTimer = false;

    public bool hasPickedTeam = false;
    public int teamID { get; set; }

    public bool testing = false;

    public string GameMode = "LivesRemaining";
    public int RedTeamLives = 50;
    public int GreenTeamLives = 50; 
    
    // Use this for initialization
    void Start()
    {

        //PhotonNetwork.offlineMode = true;
        //PhotonNetwork.CreateRoom("Offline Room");

        //PhotonNetwork.logLevel = PhotonLogLevel.Full;
        spawnSpots = FindObjectsOfType<SpawnSpot>();
        PhotonNetwork.player.name = PlayerPrefs.GetString("Username", "Player");
        chatMessages = new List<string>();

        //HealthScriptObject walls = GameObject.FindWithTag("walls").GetComponent<HealthScriptObject>();
        HealthScriptObject[] walls = FindObjectsOfType<HealthScriptObject>();
        int startIndex = 100000;
        int multiplier = 1; 
        foreach (HealthScriptObject wall in walls)
        {            
            wall.Network_InstantiateWalls(startIndex * multiplier);
            multiplier++;
        }
                
        //walls.Network_InstantiateWalls(10000);

        //HealthScriptObject borders = GameObject.FindWithTag("borders").GetComponent<HealthScriptObject>();
        //borders.Network_InstantiateWalls(12000);

        //standbyCamera = GameObject.FindGameObjectWithTag("StandbyCamera");        
        ls = GetComponent<LoadingScreen>();        
    }

    void Update()
    {
        if (respawnTimer > 0 && startTimer)
        {
            respawnTimer -= Time.deltaTime;

            if (respawnTimer <= 0)
            {
                // Time to respawn the player!
                SpawnMyPlayer(teamID);
            }
        }        
    }

    void OnGUI()
    {
        // We are fully connected, make sure to display the chat box.
        DisplayChatBox();
    }

    void DisplayChatBox()
    {

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();        

        foreach (string msg in chatMessages)
        {            
            GUILayout.Label(msg);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    void OnDestroy()
    {
        PlayerPrefs.SetString("Username", PhotonNetwork.player.name);
    }

    public void AddChatMessageLocal(string m)
    {
        AddChatMessage_RPC(m);
    }
    public void AddChatMessage(string m)
    {        
        GetComponent<PhotonView>().RPC("AddChatMessage_RPC", PhotonTargets.AllBuffered, m);
    }

    [RPC]
    void AddChatMessage_RPC(string m)
    {
        while (chatMessages.Count >= maxChatMessages)
        {
            chatMessages.RemoveAt(0);
        }
        chatMessages.Add(m);
    }

    public void Connect()
    {
        PhotonNetwork.ConnectUsingSettings("Arena v0.1");        
    }

    public void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnPhotonRandomJoinFailed");
        PhotonNetwork.CreateRoom(null);
    }

    void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom: " +  offlineMode);
        if (offlineMode == false)
        {
            ls.connecting = false;   
        }            
    }

    public void SpawnMyPlayer(int TeamID)
    {                
        teamID = TeamID;                

        if (spawnSpots == null)
        {
            Debug.LogError("No spawn spots.");
            return;
        }

        SpawnSpot mySpawnSpot = spawnSpots[Random.Range(0, spawnSpots.Length)];
        Vector3 spawnpos = new Vector3(mySpawnSpot.transform.position.x, mySpawnSpot.transform.position.y, -1);
        GameObject player = PhotonNetwork.Instantiate(character, spawnpos, Quaternion.identity, 0);
        
        PlayerScript playerscript = player.GetComponent<PlayerScript>();        
        playerscript.character = character;

        Stats stats = player.GetComponent<Stats>();
        stats.isControllable = true;

        standbyCamera.SetActive(false);        
        GameObject cam = player.transform.FindChild("Main Camera").gameObject;
        cam.SetActive(true);
        cam.GetComponent<Camera>().enabled = true;
        cam.GetComponent<AudioListener>().enabled = true; 

        HealthScript hpscript = player.GetComponent<HealthScript>();
        hpscript.enabled = true;
        hpscript.playerid = GetInstanceID();

        player.GetComponent<PhotonView>().RPC("SetTeamID", PhotonTargets.AllBuffered, TeamID);
        LowerTeamLives(TeamID);
        startTimer = false;        
        DisplayAbilityHotkeys();
        AddChatMessage(PhotonNetwork.player.name + " is respawning. Red lives: " + RedTeamLives + " Green lives: " + GreenTeamLives);
        //disable the loading screen for performance
        ls.enabled = false;        
    }

    void DisplayAbilityHotkeys()
    {        
        if (character == "Knight")
        {
            AddChatMessageLocal("(y) Increase movement speed for a short time.");
            AddChatMessageLocal("(u) Heal for a small amount.");
            AddChatMessageLocal("(i) Fortify for 50% more hp and minor damage reduction.");
        }

        if (character == "Red_Wizard")
        {
            AddChatMessageLocal("(y) Blink - Teleport a short distance.");
            AddChatMessageLocal("(u) Quick heal for a small amount.");
            AddChatMessageLocal("(i) Heal for a large amount.");            
            AddChatMessageLocal("(j) Summon a meteor to a location. Hold up to 1.5 seconds.");
            AddChatMessageLocal("(h) Fireball for moderate damage.");
            AddChatMessageLocal("(n) Convert hp to mana.");
        }

        if (character == "Druid")
        {
            AddChatMessageLocal("(y) Bird form with high movement speed for a short duration.");
            AddChatMessageLocal("(h) Bear form with high defense and slow movement.");
            AddChatMessageLocal("(j) Wolf form with average stats all around.");
            AddChatMessageLocal("(u) Healing projectile for a moderate amount.");
            AddChatMessageLocal("(i) Healing projectile for multiple targets.");
            AddChatMessageLocal("(n) Shift back to human form with a moderate heal. Must be human to shift to other forms.");
        }

        if (character == "Ranger")
        {
            AddChatMessageLocal("(1) Bow, (2) Spears, (3) Gun");
            AddChatMessageLocal("(y) Barkskin for moderate damage reduction.");
            AddChatMessageLocal("(h) Shoot two extra arrows at an angle.");
            AddChatMessageLocal("(j) Shoot five arrows rapidly.");
        }
        
    }
    void LowerTeamLives(int TeamID)
    {
        if (TeamID == 1)
        {
            if (RedTeamLives - 1 > 0)
            {
                RedTeamLives -= 1;
            }
            else
            {
                //Start new match
                Debug.Log("Green team wins.");
            }
        }

        if (TeamID == 2)
        {
            if (GreenTeamLives - 1 > 0)
            {
                GreenTeamLives -= 1;
            }
            else
            {
                //Start new match
                Debug.Log("Red team wins.");   
            }        
        }


    }    
     
}
